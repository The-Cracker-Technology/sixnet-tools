rm -rf /opt/ANDRAX/sixnet-tools

/opt/ANDRAX/python27/bin/pip2 install ipaddr

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf SIXNET-Tools /opt/ANDRAX/sixnet-tools

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
